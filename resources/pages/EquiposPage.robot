***Settings***
Documentation     Representação a pagina Equipos com seus elementos e ações


***Variables***
${EQUIPOS_FORM}        css:a[href$=register]  


***Keywords***
Customer New Equipo
  [Arguments]      ${equipamento}        ${vl_diaria}
  Input text           id:equipo-name        ${equipamento}
  Input text           id:daily_price        ${vl_diaria}
  Wait Until Element Is Not Visible     ${TOASTER_SUCCESS}      10
  Click Element        xpath://button[text()='CADASTRAR']

