***Settings***
Documentation     Representação da página login com todos os seus elementos

***Keywords***
Login With
    [Arguments]     ${email}         ${senha}
    
    Input Text       id:txtEmail                   ${email}
    Input Text       css:input[placeholder=Senha]    ${senha}
    Click Element    //button[text()='Entrar']