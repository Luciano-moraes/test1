***Settings***
Library       SeleniumLibrary

***Keywords***
Start Session
   
   Run Keyword If     "${browser}" == "headless"
   ...        Open Chrome Headless

   Run Keyword If     "${browser}" == "chrome"
   ...        Open Chrome

   set Window Size     1440     900

Finish TestCase
   Capture Page Screenshot

Finish Session
    Close Browser

Login Session 
    Start Session
    Go TO          ${base_url} 
    Login With     ${admin_user}     ${admin_pass} 

### webdriver headles
Open Chrome Headless
   Open Browser    about:blank         headlesschrome    options=add_argument('--disable-dev-shm-usage')

Open Chrome
   Open Browser  about:blank         gc   options=add_experimental_option('excludeSwitches',['enable-logging'])